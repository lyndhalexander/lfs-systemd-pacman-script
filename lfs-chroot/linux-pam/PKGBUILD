# -*-sh-*-
# Beyond Linux® From Scratch (systemd edition) - Version 2018-06-17
# Chapter 4. Security
# http://www.linuxfromscratch.org/blfs/view/systemd/postlfs/linux-pam.html
# https://git.archlinux.org/svntogit/packages.git/tree/trunk/PKGBUILD?h=packages/pam

pkgname=linux-pam
pkgver=1.3.0
pkgrel=1
pkgdesc="PAM (Pluggable Authentication Modules) library"
arch=('x86_64')
license=('GPL2')
url="http://linux-pam.org"
depends=('glibc' 'cracklib' 'libtirpc' 'pambase')
makedepends=('flex' 'w3m' 'docbook-xml>=4.4' 'docbook-xsl')
backup=(etc/security/{access.conf,group.conf,limits.conf,namespace.conf,namespace.init,pam_env.conf,time.conf} etc/default/passwd etc/environment)
groups=('security')
source=(http://linux-pam.org/library/Linux-PAM-$pkgver.tar.bz2)
md5sums=('da4b2289b7cfb19583d54e9eaaef1c3a')

options=('!emptydirs')

prepare(){
    cd $srcdir/Linux-PAM-$pkgver
    ./configure --prefix=/usr --sysconfdir=/etc --libdir=/usr/lib --enable-securedir=/lib/security --docdir=/usr/share/doc/Linux-PAM-${pkgver}
}

build() {
    cd $srcdir/Linux-PAM-$pkgver
    make
}

package() {
    cd $srcdir/Linux-PAM-$pkgver
    make DESTDIR=$pkgdir  install

    # set unix_chkpwd uid
    chmod +s $pkgdir/sbin/unix_chkpwd

    # remove doc which is not used anymore
    # FS #40749
    rm $pkgdir/usr/share/doc/Linux-PAM-${pkgver}/sag-pam_userdb.html
}
