# -*-sh-*-
# Beyond Linux® From Scratch (systemd edition) - Version 2018-06-17
# Chapter 4. Security
# http://www.linuxfromscratch.org/blfs/view/systemd/postlfs/openssh.html
# https://git.archlinux.org/svntogit/packages.git/tree/trunk/PKGBUILD?h=packages/openssh

pkgname=openssh
pkgver=7.7p1
pkgrel=1
pkgdesc='Premier connectivity tool for remote login with the SSH protocol'
url='https://www.openssh.com/portable.html'
license=('custom:BSD')
arch=('x86_64')
makedepends=()
depends=('krb5' 'openssl' 'libedit')
optdepends=('xorg-xauth: X11 forwarding'
            'x11-ssh-askpass: input passphrase in X')
validpgpkeys=('59C2118ED206D927E667EBE3D3E5F56B6D920D30')
source=("https://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/${pkgname}-${pkgver}.tar.gz"{,.asc}
        'openssl-1.1.0.patch'
        'sshdgenkeys.service'
        'sshd@.service'
        'sshd.service'
        'sshd.socket'
        'sshd.conf')

backup=('etc/ssh/ssh_config' 'etc/ssh/sshd_config')

_confargs=()
if [[ -z "$(pacman -Qq linux-headers 2>&1 >/dev/null)" ]];then
    makedepends+=( 'linux-headers')
fi

if [[ -z "$(pacman -Qq xorg-xauth 2>&1 >/dev/null)" ]];then
    _confargs+=( '--with-xauth=/usr/bin/xauth')
fi

if [[ -z "$(pacman -Qq ldns 2>&1 >/dev/null)" ]];then
    _confargs+=( '--with-ldns')
    depends+=( 'ldns')
fi

prepare() {
    cd "${srcdir}/${pkgname}-${pkgver}"

    # OpenSSL 1.1.0 patch from http://vega.pgw.jp/~kabe/vsd/patch/openssh-7.4p1-openssl-1.1.0c.patch.html
    patch -p1 -i ../openssl-1.1.0.patch
    ./configure \
	--prefix=/usr \
	--libexecdir=/usr/lib/ssh \
	--sysconfdir=/etc/ssh \
	--with-libedit \
	--with-ssl-engine \
	--with-pam \
	--with-privsep-user=nobody \
	--with-kerberos5=/usr \
	--with-md5-passwords \
	--with-pid-dir=/run \
	--with-default-path='/usr/local/sbin:/usr/local/bin:/usr/bin' \
        ${_confargs[@]}
}

build() {
    cd "${srcdir}/${pkgname}-${pkgver}"
    make
}

check() {
    cd "${srcdir}/${pkgname}-${pkgver}"

    # Tests require openssh to be already installed system-wide,
    # also connectivity tests will fail under makechrootpkg since
    # it runs as nobody which has /bin/false as login shell.

    if [[ -e /usr/bin/scp && ! -e /.arch-chroot ]]; then
	make tests
    fi
}

package() {
    cd "${srcdir}/${pkgname}-${pkgver}"

    make DESTDIR="${pkgdir}" install

    ln -sf ssh.1.gz "${pkgdir}"/usr/share/man/man1/slogin.1.gz
    install -Dm644 LICENCE "${pkgdir}/usr/share/licenses/${pkgname}/LICENCE"

    install -Dm644 ../sshdgenkeys.service "${pkgdir}"/lib/systemd/system/sshdgenkeys.service
    install -Dm644 ../sshd@.service "${pkgdir}"/lib/systemd/system/sshd@.service
    install -Dm644 ../sshd.service "${pkgdir}"/lib/systemd/system/sshd.service
    install -Dm644 ../sshd.socket "${pkgdir}"/lib/systemd/system/sshd.socket
    install -Dm644 ../sshd.conf "${pkgdir}"/usr/lib/tmpfiles.d/sshd.conf

    install -Dm755 contrib/findssl.sh "${pkgdir}"/usr/bin/findssl.sh
    install -Dm755 contrib/ssh-copy-id "${pkgdir}"/usr/bin/ssh-copy-id
    install -Dm644 contrib/ssh-copy-id.1 "${pkgdir}"/usr/share/man/man1/ssh-copy-id.1

    sed \
	-e '/^#ChallengeResponseAuthentication yes$/c ChallengeResponseAuthentication no' \
	-e '/^#PrintMotd yes$/c PrintMotd no # pam does that' \
	-e '/^#UsePAM no$/c UsePAM yes' \
	-i "${pkgdir}"/etc/ssh/sshd_config
}
md5sums=('68ba883aff6958297432e5877e9a0fe2'
         'SKIP'
         '0030d47f1efdecb455f7ccb0b9e633b1'
         'f7c35eab2ca1ea6352da19eb2e1f6b6b'
         '51284b11c80c2022c64015084eaf77d2'
         '36db016390876526e5361f7c1d0ae169'
         '76f52c66fb3193f301fe0a666e047ab1'
         'bce5e4a148156b5424112c48de2b243e')
