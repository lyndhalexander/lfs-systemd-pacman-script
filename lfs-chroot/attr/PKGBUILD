# -*-sh-*-
# Linux From Scratch - Version 20180612-systemd
# Chapter 6. Installing Basic System Software
# http://www.linuxfromscratch.org/lfs/view/systemd/chapter06/attr.html
# https://git.archlinux.org/svntogit/packages.git/tree/trunk/PKGBUILD?h=packages/attr

pkgname=attr
pkgver=2.4.47
pkgrel=1
pkgdesc="Extended attribute support library for ACL support"
arch=('x86_64')
url="http://savannah.nongnu.org/projects/attr"
license=('LGPL')
depends=('glibc')
makedepends=($(pacman -Qq gettext 2>/dev/null && echo 'gettext'))
groups=('core')
replaces=('xfsattr')
provides=('xfsattr')
conflicts=('xfsattr')
source=(https://download.savannah.gnu.org/releases/$pkgname/$pkgname-$pkgver.src.tar.gz{,.sig})
validpgpkeys=('600CD204FBCEA418BD2CA74F154343260542DF34') # Brandon Philips

prepare(){
    cd $pkgname-$pkgver
    export INSTALL_USER=root INSTALL_GROUP=root

    # Modify the documentation directory so that it is a versioned directory
    sed -i -e 's|/@pkg_name@|&-@pkg_version@|' include/builddefs.in

    # Prevent installation of manual pages that were already installed by the man pages package
    sed -i -e "/SUBDIRS/s|man[25]||g" man/Makefile

    # Fix a problem in the test procedures caused by changes in perl-5.26
    sed -i 's:{(:\\{(:' test/run

    ./configure --prefix=/usr --disable-static
}

build() {
    cd $pkgname-$pkgver
    make
}

package() {
    cd $pkgname-$pkgver
    make DIST_ROOT="$pkgdir" install install-lib install-dev

    # tidy up
    chmod 0755 "$pkgdir"/usr/lib/libattr.so.*.*.*
    rm -rf "$pkgdir"/usr/share/man/man2

    install -v -dm755 "$pkgdir"/lib
    mv -v "$pkgdir"/usr/lib/libattr.so.* "$pkgdir"/lib
    ln -sfv ../../lib/$(readlink "$pkgdir"/usr/lib/libattr.so) "$pkgdir"/usr/lib/libattr.so
}
sha256sums=('25772f653ac5b2e3ceeb89df50e4688891e21f723c460636548971652af0a859'
            'SKIP')
