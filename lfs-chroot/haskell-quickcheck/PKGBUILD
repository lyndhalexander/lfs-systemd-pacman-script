# -*-sh-*-

_hkgname=QuickCheck
pkgname=haskell-quickcheck
pkgver=2.11.3
pkgrel=1
pkgdesc='Automatic testing of Haskell programs'
url='https://hackage.haskell.org/package/QuickCheck'
license=('custom:BSD3')
arch=('x86_64')
depends=('ghc-libs' 'haskell-random' 'haskell-tf-random')
makedepends=('ghc')
source=("https://hackage.haskell.org/packages/archive/$_hkgname/$pkgver/$_hkgname-$pkgver.tar.gz")
sha512sums=('17b3c5803cbca980375a1837b4ba931f346be8a720fcc0e37ad2c46abc8ba2073c49635bc89739d34653376c3f7fe1bd39560092c005b8dbce0a7effac25d73d')

build() {
    cd "$srcdir/$_hkgname-$pkgver"

    runhaskell Setup configure \
               -O --enable-shared --enable-executable-dynamic --disable-library-vanilla \
               --prefix=/usr --docdir="/usr/share/doc/$pkgname" \
               --dynlibdir=/usr/lib --libsubdir=\$compiler/site-local/\$pkgid \
               -ftemplateHaskell

    runhaskell Setup build
    runhaskell Setup register --gen-script
    runhaskell Setup unregister --gen-script

    sed -i -r -e "s|ghc-pkg.*update[^ ]* |&'--force' |" register.sh
    sed -i -r -e "s|ghc-pkg.*unregister[^ ]* |&'--force' |" unregister.sh
}

package() {
    cd "$srcdir/$_hkgname-$pkgver"

    install -D -m744 register.sh "$pkgdir/usr/share/haskell/register/$pkgname.sh"
    install -D -m744 unregister.sh "$pkgdir/usr/share/haskell/unregister/${pkgname}.sh"
    runhaskell Setup copy --destdir="$pkgdir"
    install -D -m644 "LICENSE" "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
    rm -f "$pkgdir/usr/share/doc/$pkgname/LICENSE"
}

# vim: ts=2 sw=2 et:
