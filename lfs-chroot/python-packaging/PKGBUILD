# -*-sh-*-

pkgbase=python-packaging
pkgname=(python-packaging python2-packaging)
pkgver=17.1
_commit=0dfec396998e7237e12dc2e3a06385ffa1bd97b1
pkgrel=1
pkgdesc="Core utilities for Python packages"
arch=('any')
url="https://github.com/pypa/packaging"
license=('Apache')
makedepends=()
checkdepends=()
source=("$pkgbase-$_commit.tar.gz::https://github.com/pypa/packaging/archive/$_commit.tar.gz")
sha512sums=('00f0ac87794813372931b31c5dd52f76153b0fa8fc21c2a1d5dafdd792e12ef0d87503aa7bbceb8642f777900dcaccfafca77dc391c5afdce1471a7ba9c4021e')

if [[ -z "$(pacman -Qq python-setuptools 2>&1 >/dev/null && echo 'python-setuptools')" ]];then
    makedepends+=( 'python-setuptools')
fi
if [[ -z "$(pacman -Qq python2-setuptools 2>&1 >/dev/null && echo 'python2-setuptools')" ]];then
    makedepends+=( 'python2-setuptools')
fi

if [[ -z "$(pacman -Qq python-pyparsing 2>&1 >/dev/null && echo 'python-pyparsing')" ]];then
    makedepends+=( 'python-pyparsing')
fi
if [[ -z "$(pacman -Qq python2-pyparsing 2>&1 >/dev/null && echo 'python2-pyparsing')" ]];then
    makedepends+=( 'python2-pyparsing')
fi

if [[ -z "$(pacman -Qq python-pytest-runner 2>&1 >/dev/null && echo 'python-pytest-runner')" ]];then
    makedepends+=( 'python-pytest-runner')
fi
if [[ -z "$(pacman -Qq python2-pytest-runner 2>&1 >/dev/null && echo 'python2-pytest-runner')" ]];then
    makedepends+=( 'python2-pytest-runner')
fi

if [[ -z "$(pacman -Qq python-pretend 2>&1 >/dev/null && echo 'python-pretend')" ]];then
    makedepends+=( 'python-pretend')
fi
if [[ -z "$(pacman -Qq python2-pretend 2>&1 >/dev/null && echo 'python2-pretend')" ]];then
    makedepends+=( 'python2-pretend')
fi

if [[ -z "$(pacman -Qq python-coverage 2>&1 >/dev/null && echo 'python-coverage')" ]];then
    makedepends+=( 'python-coverage')
fi
if [[ -z "$(pacman -Qq python2-coverage 2>&1 >/dev/null && echo 'python2-coverage')" ]];then
    makedepends+=( 'python2-coverage')
fi

prepare() {
    mv packaging-{$_commit,$pkgver}
    cp -a packaging-$pkgver{,-py2}
}

build() {
    cd "$srcdir"/packaging-$pkgver
    python setup.py build

    cd "$srcdir"/packaging-$pkgver-py2
    python2 setup.py build
}

check() {
    cd "$srcdir"/packaging-$pkgver
    python setup.py pytest

    cd "$srcdir"/packaging-$pkgver-py2
    python2 setup.py pytest
}

package_python-packaging() {
    depends=()

    if [[ -z "$(pacman -Qq python-pyparsing 2>&1 >/dev/null && echo 'python-pyparsing')" ]];then
        depends+=( 'python-pyparsing')
    fi
    if [[ -z "$(pacman -Qq python-six 2>&1 >/dev/null && echo 'python-six')" ]];then
        depends+=( 'python-six')
    fi

    cd "$srcdir"/packaging-$pkgver
    python setup.py install --root "$pkgdir"
}

package_python2-packaging() {
    depends=()

    if [[ -z "$(pacman -Qq python2-pyparsing 2>&1 >/dev/null && echo 'python2-pyparsing')" ]];then
        depends+=( 'python2-pyparsing')
    fi
    if [[ -z "$(pacman -Qq python2-six 2>&1 >/dev/null && echo 'python2-six')" ]];then
        depends+=( 'python2-six')
    fi

    cd "$srcdir"/packaging-$pkgver-py2
    python2 setup.py install --root "$pkgdir"
}
