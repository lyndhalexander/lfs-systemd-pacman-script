# -*-sh-*-
# Linux From Scratch - Version 20180612-systemd
# Chapter 6. Installing Basic System Software
# http://www.linuxfromscratch.org/lfs/view/systemd/chapter06/acl.html
# https://git.archlinux.org/svntogit/packages.git/tree/trunk/PKGBUILD?h=packages/acl

pkgname=acl
pkgver=2.2.52
pkgrel=1
pkgdesc="Access control list utilities, libraries and headers"
arch=('x86_64')
url="http://savannah.nongnu.org/projects/acl"
license=('LGPL')
depends=('attr>=2.4.46')
replaces=('xfsacl')
provides=('xfsacl')
conflicts=('xfsacl')
source=(https://download.savannah.gnu.org/releases/$pkgname/$pkgname-$pkgver.src.tar.gz{,.sig})
validpgpkeys=('600CD204FBCEA418BD2CA74F154343260542DF34') # Brandon Philips
sha256sums=('179074bb0580c06c4b4137be4c5a92a701583277967acdb5546043c7874e0d23'
            'SKIP')

prepare(){
    cd $pkgname-$pkgver

    # Modify the documentation directory so that it is a versioned directory
    sed -i -e 's|/@pkg_name@|&-@pkg_version@|' include/builddefs.in

    # Fix some broken tests
    sed -i "s:| sed.*::g" test/{sbits-restore,cp,misc}.test

    # Fix a problem in the test procedures caused by changes in perl-5.26
    sed -i 's/{(/\\{(/' test/run

    # fix a bug that causes getfacl -e to segfault on overly long group name
    sed -i -e "/TABS-1;/a if (x > (TABS-1)) x = (TABS-1);" \
        libacl/__acl_to_any_text.c

    export INSTALL_USER=root INSTALL_GROUP=root
    ./configure --prefix=/usr --libexecdir=/usr/lib --disable-static
    
}
build() {
    cd $pkgname-$pkgver
    make
}

package() {
    cd $pkgname-$pkgver
    make DIST_ROOT="$pkgdir" install install-lib install-dev

    install -v -dm755 "$pkgdir"/lib
    mv -v "$pkgdir"/usr/lib/libacl.so.* "$pkgdir"/lib
    ln -sfv ../../lib/$(readlink "$pkgdir"/usr/lib/libacl.so) "$pkgdir"/usr/lib/libacl.so
}
