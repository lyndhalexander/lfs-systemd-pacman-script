# -*-sh-*-
# https://git.archlinux.org/svntogit/community.git/tree/trunk/PKGBUILD?h=packages/libutf8proc

pkgname=libutf8proc
pkgver=1.3.1_3
pkgrel=2
pkgdesc='C library for processing UTF-8 encoded Unicode strings'
arch=('x86_64')
url='https://git.netsurf-browser.org/libutf8proc.git/'
license=('MIT' 'custom')
makedepends=('cmake' $(pacman -Qq git 2>/dev/null && echo 'git') 'ninja' 'setconf')
groups=('general-libraries')
source=("http://download.netsurf-browser.org/libs/releases/libutf8proc-${pkgver/_/-}-src.tar.gz"
        'libutf8proc.pc')
sha256sums=('1223c2dd9c5cd077564feb30a6ad8b1c0e55ef5dfe7c275d3b9fe8274f207c70'
            '345ea6782c361f5d38808c13e1e6ce6c2bef69a22a97d115b8ce950d74de1ed1')

prepare() {
    setconf libutf8proc.pc Version "${pkgver%%_*}"

    cd "$pkgname-${pkgver/_/-}"
    find . -iwholename '*cmake*' \
         -not -name CMakeLists.txt \
         -not -name utils.cmake \
         -delete
    rm -fv Makefile
    cp -r src/* include/$pkgname/* .
    mkdir -p build
    cd build
    cmake .. \
          -DCMAKE_INSTALL_PREFIX=/usr \
          -DBUILD_SHARED_LIBS=ON \
          -G Ninja
}

build() {
    cd "$pkgname-${pkgver/_/-}"

    cd build
    ninja
}

package() {
    cd "$pkgname-${pkgver/_/-}"

    # The CMake/install configuration does not work for libutf8proc
    # DESTDIR="$pkgdir" ninja -C "$pkgname-$pkgver/build" install

    install -Dm644 utf8proc.h "$pkgdir/usr/include/utf8proc.h"
    install -Dm644 LICENSE.md "$pkgdir/usr/share/licenses/$pkgname/LICENSE.md"
    install -Dm644 "$srcdir/libutf8proc.pc" \
            "$pkgdir/usr/lib/pkgconfig/libutf8proc.pc"
    install -Dm644 build/libutf8proc.so.1.3.1 \
            "$pkgdir/usr/lib/libutf8proc.so.1.3.1"

    ln -s /usr/lib/libutf8proc.so.1.3.1 "$pkgdir/usr/lib/libutf8proc.so.1"
    ln -s /usr/lib/libutf8proc.so.1.3.1 "$pkgdir/usr/lib/libutf8proc.so"
}

# getver: git.netsurf-browser.org/libutf8proc.git/
# vim: ts=2 sw=2 et:
