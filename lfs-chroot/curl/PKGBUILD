# -*-sh-*-
# Beyond Linux® From Scratch (systemd edition) - Version 2018-06-15
# Chapter 17. Networking Libraries
# http://www.linuxfromscratch.org/blfs/view/systemd/basicnet/curl.html
# https://git.archlinux.org/svntogit/packages.git/tree/trunk/PKGBUILD?h=packages/curl

pkgname=curl
pkgver=7.60.0
pkgrel=1
pkgdesc="An URL retrieval utility and library"
arch=('x86_64')
url="https://curl.haxx.se"
license=('MIT')
depends=('make-ca' 'krb5' 'libssh2' 'openssl' 'zlib' 'libnghttp2')
provides=('libcurl.so')
options=('strip')
groups=('networking-libraries')
source=("https://curl.haxx.se/download/$pkgname-$pkgver.tar.gz"{,.asc})
sha512sums=('f25c8d79be87bfbcae93cd200b319f664efd62aea8f1a94bb441407a9e1489bd935943cfd1347f3b252f94b9a0286da8aeb04b407a2ba95ebfa717dff80e891b'
            'SKIP')
validpgpkeys=('27EDEAF22F3ABCEB50DB9A125CC908FDB71E12C2'   # Daniel Stenberg
              '914C533DF9B2ADA2204F586D78E11C6B279D5C91')  # Daniel Stenberg (old key)

if [[ -z "$(pacman -Qq libpsl 2>&1 >/dev/null)" ]];then
    depends+=( 'libpsl')
fi

prepare(){
    cd "$pkgname-$pkgver"
    ./configure \
        --prefix=/usr \
        --mandir=/usr/share/man \
        --disable-ldap \
        --disable-ldaps \
        --disable-manual \
        --enable-ipv6 \
        --enable-versioned-symbols \
        --enable-threaded-resolver \
        --with-gssapi \
        --with-random=/dev/urandom \
        --with-ca-path=/etc/ssl/certs
}

build() {
    cd "$pkgname-$pkgver"
    make
}

package() {
    cd "$pkgname-$pkgver"

    make DESTDIR="$pkgdir" install
    make DESTDIR="$pkgdir" install -C scripts

    # license
    install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
