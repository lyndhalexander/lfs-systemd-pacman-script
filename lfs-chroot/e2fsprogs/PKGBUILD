# -*-sh-*-
#  Linux From Scratch - Version 20180612-systemd
# Chapter 6. Installing Basic System Software
# http://www.linuxfromscratch.org/lfs/view/systemd/chapter06/e2fsprogs.html
# https://git.archlinux.org/svntogit/packages.git/tree/trunk/PKGBUILD?h=packages/e2fsprogs

pkgname=e2fsprogs
pkgver=1.44.2
pkgrel=1
pkgdesc="Ext2/3/4 filesystem utilities"
arch=('x86_64')
license=('GPL' 'LGPL' 'MIT')
url="http://e2fsprogs.sourceforge.net"
groups=('base')
depends=('sh' $(pacman -Qq libutil-linux 2>/dev/null && echo'libutil-linux'))
makedepends=($(pacman -Qq util-linux 2>/dev/null && echo 'util-linux'))
backup=('etc/mke2fs.conf')
options=('staticlibs')
validpgpkeys=('3AB057B7E78D945C8C5591FBD36F769BC11804F0') # Theodore Ts'o <tytso@mit.edu>
source=("https://www.kernel.org/pub/linux/kernel/people/tytso/${pkgname}/v${pkgver}/${pkgname}-${pkgver}.tar."{xz,sign})

prepare() {
    cd "${srcdir}/${pkgname}-${pkgver}"

    # Remove unnecessary init.d directory
    sed -i '/init\.d/s|^|#|' misc/Makefile.in

    if [[ -n "$(pacman -Qq util-linux 2>&1 >/dev/null)" ]];then
        LIBS="${LIBS} -L/tools/lib"
        CFLAGS="${CFLAGS} -I/tools/include"
        PKG_CONFIG_PATH="${PKG_CONFIG_PATH}:/tools/lib/pkgconfig"
    fi

    ./configure \
        --prefix=/usr \
        --bindir=/bin \
        --with-root-prefix="" \
        --enable-elf-shlibs \
        --disable-libblkid \
        --disable-libuuid \
        --disable-uuidd \
        --disable-fsck
}

build() {
    cd "${srcdir}/${pkgname}-${pkgver}"
    make
}

package() {
    unset MAKEFLAGS

    cd "${srcdir}/${pkgname}-${pkgver}"
    make DESTDIR="${pkgdir}" install install-libs

    chmod -v u+w "${pkgdir}"/usr/lib/{libcom_err,libe2p,libext2fs,libss}.a

    sed -i -e 's/^AWK=.*/AWK=awk/' "${pkgdir}/bin/compile_et"

    # remove references to build directory
    sed -i -e 's#^SS_DIR=.*#SS_DIR="/usr/share/ss"#' "${pkgdir}/bin/mk_cmds"
    sed -i -e 's#^ET_DIR=.*#ET_DIR="/usr/share/et"#' "${pkgdir}/bin/compile_et"

    # remove static libraries with a shared counterpart
    rm "${pkgdir}"/usr/lib/lib{com_err,e2p,ext2fs,ss}.a
}

md5sums=('f8389455f5a010acbdc72f11c4d487dd'
         'SKIP')
