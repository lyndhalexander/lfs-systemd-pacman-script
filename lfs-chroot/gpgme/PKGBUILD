# -*-sh-*-
# Beyond Linux® From Scratch (systemd edition) - Version 2018-06-15
# Chapter 4. Security
# http://www.linuxfromscratch.org/blfs/view/systemd/postlfs/gpgme.html
# https://git.archlinux.org/svntogit/packages.git/tree/trunk/PKGBUILD?h=packages/gpgme

pkgbase=gpgme
pkgname=(gpgme python-gpgme python2-gpgme)
pkgver=1.11.1
pkgrel=1
pkgdesc="A C wrapper library for GnuPG"
arch=('x86_64')
url="http://www.gnupg.org/related_software/gpgme/"
license=('LGPL')
makedepends=('libgpg-error' 'gnupg' 'python' 'python2')
groups=('security')
source=("https://www.gnupg.org/ftp/gcrypt/${pkgbase}/${pkgbase}-${pkgver}.tar.bz2"{,.sig})
sha256sums=('2d1b111774d2e3dd26dcd7c251819ce4ef774ec5e566251eb9308fa7542fbd6f'
            'SKIP')
validpgpkeys=('D8692123C4065DEA5E0F3AB5249B39D24F25E3B6') # Werner Koch

if [[ -z "$(pacman -Qq qt5-base 2>&1 >/dev/null)" ]];then
    pkgname+=( 'qgpgme')
    makedepends+=( 'qt5-base')
    _QT=1
fi
if [[ -z "$(pacman -Qq swig 2>&1 >/dev/null)" ]];then
    makedepends+=( 'swig')
fi
    
prepare(){
    cd ${pkgbase}-${pkgver}
    ./configure \
        --prefix=/usr \
        --disable-fd-passing \
        --disable-static \
        --disable-gpgsm-test
}

build() {
    cd ${pkgbase}-${pkgver}
    make
}

check() {
    cd ${pkgbase}-${pkgver}
    make check || true
}

package_gpgme() {
    depends=('libgpg-error' 'gnupg>=2')
    options=('!emptydirs')

    cd ${pkgbase}-${pkgver}
    make DESTDIR="${pkgdir}" install

    # split qgpgme
    [[ -d "${pkgdir}"/usr/include/qgpgme/ ]] && rm -r "${pkgdir}"/usr/include/qgpgme/
    [[ -d "${pkgdir}"/usr/include/qgpgme/ ]] && rm -r "${pkgdir}"/usr/include/QGpgME/
    [[ -d "${pkgdir}"/usr/lib/cmake/QGpgme ]] && rm -r "${pkgdir}"/usr/lib/cmake/QGpgme

    # https://stackoverflow.com/questions/2937407/test-whether-a-glob-matches-any-files
    if compgen -G "${pkgdir}/usr/lib/libqgpgme.*";then
        rm -r "${pkgdir}"/usr/lib/libqgpgme.*
    fi
    if compgen -G "${pkgdir}/usr/lib/python*";then
        rm -r "${pkgdir}"/usr/lib/libqgpgme.*
    fi
}

package_qgpgme() {
    pkgdesc="Qt bindings for GPGme"
    depends=('gpgme' 'qt5-base')

    cd ${pkgbase}-${pkgver}/lang/qt
    make DESTDIR="${pkgdir}" install
}

package_python-gpgme() {
    pkgdesc="Python bindings for GPGme"
    depends=('gpgme' 'python')

    cd ${pkgbase}-${pkgver}/lang/python
    make DESTDIR="${pkgdir}" install
    rm -rf "${pkgdir}/usr/lib"/python2*/
}

package_python2-gpgme() {
    pkgdesc="Python 2 bindings for GPGme"
    depends=('gpgme' 'python2')

    cd ${pkgbase}-${pkgver}/lang/python
    make DESTDIR="${pkgdir}" install
    rm -rf "${pkgdir}/usr/lib"/python3*/
}
