# -*-sh-*-
# Beyond Linux® From Scratch (systemd edition) - Version 2018-06-12
# Chapter 9. General Libraries
# http://www.linuxfromscratch.org/blfs/view/systemd/general/pcre.html
# https://git.archlinux.org/svntogit/packages.git/tree/trunk/PKGBUILD?h=packages/pcre

pkgname=pcre
pkgver=8.42
pkgrel=1
pkgdesc='A library that implements Perl 5-style regular expressions'
arch=('x86_64')
url='http://www.pcre.org/'
license=('BSD')
depends=('gcc-libs' 'readline' 'zlib' 'bzip2' 'bash')
validpgpkeys=('45F68D54BBE23FB3039B46E59766E084FB0F43D8') # Philip Hazel
source=("https://ftp.pcre.org/pub/pcre/$pkgname-$pkgver.tar.bz2"{,.sig})
md5sums=('085b6aa253e0f91cae70b3cdbe8c1ac2'
         'SKIP')

prepare() {
    cd $pkgname-$pkgver
    ./configure \
        --prefix=/usr \
        --docdir=/usr/share/doc/${pkgname}-${pkgver} \
        --enable-unicode-properties \
        --enable-pcre16 \
        --enable-pcre32 \
        --enable-jit \
        --enable-pcregrep-libz \
        --enable-pcregrep-libbz2 \
        --enable-pcretest-libreadline
}

build() {
    cd $pkgname-$pkgver
    make
}

check() {
    cd $pkgname-$pkgver
    make -j1 check
}

package() {
    cd $pkgname-$pkgver
    make DESTDIR="$pkgdir" install

    install -v -dm755 "$pkgdir"/lib
    mv -v  "$pkgdir"/usr/lib/libpcre.so.*  "$pkgdir"/lib
    ln -sfv ../../lib/$(readlink  "$pkgdir"/usr/lib/libpcre.so)  "$pkgdir"/usr/lib/libpcre.so

    install -Dm644 LICENCE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

# vim:set ts=2 sw=2 et:

