# -*-sh-*-
# Linux From Scratch - Version 20180612-systemd
# Chapter 6. Installing Basic System Software
# http://www.linuxfromscratch.org/lfs/view/systemd/chapter06/systemd.html
# https://git.archlinux.org/svntogit/packages.git/tree/trunk/PKGBUILD?h=packages/systemd

pkgbase=systemd
pkgname=('systemd' 'libsystemd' 'systemd-sysvcompat')
pkgver=238
pkgrel=1
arch=('x86_64')
url="https://www.github.com/systemd/systemd"
depends=('acl'
         $(pacman -Qq cryptsetup 2>/dev/null && echo 'cryptsetup')
         $(pacman -Qq 'docbook-xsl' 2>/dev/null && echo 'docbook-xsl')
         $(pacman -Qq 'gperf' 2>/dev/null && echo 'gperf')
         $(pacman -Qq 'lz4' 2>/dev/null && echo 'lz4')
         $(pacman -Qq 'xz' 2>/dev/null && echo 'xz')
         $(pacman -Qq 'linux-pam' 2>/dev/null && echo 'linux-pam')
         'libelf'
         'intltool'
         $(pacman -Qq 'iptables' 2>/dev/null && echo 'iptables')
         'kmod'
         'libcap'
         $(pacman -Qq 'libidn' 2>/dev/null && echo 'libidn')
         $(pacman -Qq 'libidn2' 2>/dev/null && echo 'libidn2')
         $(pacman -Qq 'libgcrypt' 2>/dev/null && echo 'libgcrypt')
         $(pacman -Qq 'libmicrohttpd' 2>/dev/null && echo 'libmicrohttpd')
         $(pacman -Qq 'libxslt' 2>/dev/null && echo 'libxslt')
         $(pacman -Qq 'util-linux' 2>/dev/null && echo 'util-linux')
         'linux-api-headers'
         $(pacman -Qq 'python-lxml' 2>/dev/null && echo 'python-lxml')
         $(pacman -Qq 'quota-tools' 2>/dev/null && echo 'quota-tools')
         'shadow'
         $(pacman -Qq 'gnu-efi-libs' 2>/dev/null && echo 'gnu-efi-libs')
         $(pacman -Qq 'git' 2>/dev/null && echo 'git')
         'meson'
         $(pacman -Qq 'libseccomp' 2>/dev/null && echo 'libseccomp')
         $(pacman -Qq 'pcre2' 2>/dev/null && echo 'pcre2')
         $(pacman -Qq 'curl' 2>/dev/null && echo 'curl')
         $(pacman -Qq 'qrencode' 2>/dev/null && echo 'qrencode')
         $(pacman -Qq 'hwids' 2>/dev/null && echo 'hwids')
         $(pacman -Qq 'dbus' 2>/dev/null && echo 'dbus')
         $(pacman -Qq 'iptables' 2>/dev/null && echo 'iptables')
         $(pacman -Qq 'kbd' 2>/dev/null && echo 'kbd')
         $(pacman -Qq 'libgcrypt' 2>/dev/null && echo 'libgcrypt')
         'libelf'
         'xz'
        )
options=('strip')
source=(https://github.com/systemd/systemd/archive/v${pkgver}/systemd-${pkgver}.tar.gz
        http://anduin.linuxfromscratch.org/LFS/systemd-man-pages-${pkgver}.tar.xz
        'fe0a6b47c2f1d744e3130897cf1fe9c32fa73e78.patch'
        'systemd-user.pam'
        'systemd-hook'
        'systemd-binfmt.hook'
        'systemd-catalog.hook'
        'systemd-daemon-reload.hook'
        'systemd-hwdb.hook'
        'systemd-sysctl.hook'
        'systemd-sysusers.hook'
        'systemd-tmpfiles.hook'
        'systemd-udev-reload.hook'
        'systemd-update.hook'
        'systemd-user-sessions'
       )
noextract=(systemd-man-pages-${pkgver}.tar.xz)

prepare() {
    cd systemd-${pkgver}

    patch -Np1 -i ../fe0a6b47c2f1d744e3130897cf1fe9c32fa73e78.patch
    tar -xf ../systemd-man-pages-${pkgver}.tar.xz
    # Remove tests that cannot be built in chroot
    sed '171,$ d' -i src/resolve/meson.build

    # fixes from upstream
    sed -i '527,565 d' src/basic/missing.h
    sed -i '24 d' src/core/load-fragment.c
    sed -i '53 a#include <sys/mount.h>' src/shared/bus-unit-util.c

    # Remove an unneeded group
    sed -i 's/GROUP="render", //' rules/50-udev-default.rules.in

    meson_options=(
        --prefix=/usr
        --sysconfdir=/etc
        --localstatedir=/var
        -Dblkid=true
        -Dbuildtype=release
        -Ddefault-dnssec=no
        -Dfirstboot=false
        -Dinstall-tests=false
        -Dkill-path=/bin/kill
        -Dkmod-path=/bin/kmod
        -Dldconfig=false
        -Dmount-path=/bin/mount
        -Drootprefix=
        -Drootlibdir=/lib
        -Dsplit-usr=true
        -Dsulogin-path=/sbin/sulogin
        -Dumount-path=/bin/umount
        -Db_lto=false
    )

    if [[ -z "$(pacman -Qq docbook-xsl 2>&1 >/dev/null)" ]];then
        meson_options+=( '-Dman=true')
    else
        meson_options+=( '-Dman=false')
    fi
    
    cd $srcdir
    meson systemd-${pkgver} build "${meson_options[@]}"
}

build(){
    ninja -C build
    
}

check(){
    cd build
    meson test || true
}

package_systemd() {
    pkgdesc="system and service manager"
    license=('GPL2' 'LGPL2.1')
    groups=('core')
    depends=('acl' 'bash' 'kmod' 'libcap' 'libsystemd' 'libelf'
            )
    provides=('nss-myhostname' "systemd-tools=$pkgver" "udev=$pkgver")
    replaces=('nss-myhostname' 'systemd-tools' 'udev')
    conflicts=('nss-myhostname' 'systemd-tools' 'udev')
    optdepends=('libmicrohttpd: remote journald capabilities'
                'quota-tools: kernel-level quota management'
                'systemd-sysvcompat: symlink package to provide sysvinit binaries'
                'polkit: allow administration as unprivileged user')
    backup=(etc/pam.d/systemd-user
            etc/systemd/coredump.conf
            etc/systemd/journald.conf
            etc/systemd/journal-remote.conf
            etc/systemd/journal-upload.conf
            etc/systemd/logind.conf
            etc/systemd/system.conf
            etc/systemd/timesyncd.conf
            etc/systemd/resolved.conf
            etc/systemd/user.conf
            etc/udev/udev.conf)
    install=systemd.install

    DESTDIR=${pkgdir} ninja -C build install

    # don't write units to /etc by default. some of these will be re-enabled on
    # post_install.
    rm -rv "$pkgdir"/etc/systemd/system/*
    
    # we'll create this on installation
    [[ -d "$pkgdir"/var/log/journal/remote ]] && rm -rv "$pkgdir"/var/log/journal/remote

    # runtime libraries shipped with libsystemd
    install -v -dm755 libsystemd
    mv -v "$pkgdir"/lib/lib{nss,systemd,udev}*.so* libsystemd

    # manpages shipped with systemd-sysvcompat
    [[ -d "$pkgdir"/usr/share/man/man8 ]] && rm -v "$pkgdir"/usr/share/man/man8/{halt,poweroff,reboot,runlevel,shutdown,telinit}.8

    # executable (symlinks) shipped with systemd-sysvcompat
    rm -v "$pkgdir"/sbin/{halt,init,poweroff,reboot,runlevel,shutdown,telinit}

    # avoid a potential conflict with [core]/filesystem
    rm -v "$pkgdir"/usr/share/factory/etc/nsswitch.conf
    sed -i '/^C \/etc\/nsswitch\.conf/d' "$pkgdir"/usr/lib/tmpfiles.d/etc.conf

    # add back tmpfiles.d/legacy.conf, normally omitted without sysv-compat
    install -v -m644 systemd-${pkgver}/tmpfiles.d/legacy.conf "$pkgdir"/usr/lib/tmpfiles.d

    # ship default policy to leave services disabled
    echo 'disable *' >"$pkgdir"/lib/systemd/system-preset/99-default.preset

    # ensure proper permissions for /var/log/journal
    # The permissions are stored with named group by tar, so this works with
    # users and groups populated by systemd-sysusers. This is only to prevent a
    # warning from pacman as permissions are set by systemd-tmpfiles anyway.
    #install -v -d -o root -g systemd-journal -m 2755 "$pkgdir"/var/log/journal

    # match directory owner/group and mode from [extra]/polkit
    # install -v -d -o root -g 102 -m 750 "$pkgdir"/usr/share/polkit-1/rules.d

    # pacman hooks
    install -Dm755 systemd-hook "$pkgdir"/usr/share/libalpm/scripts/systemd-hook
    install -Dm644 -t "$pkgdir"/usr/share/libalpm/hooks *.hook

    # overwrite the systemd-user PAM configuration with our own
    install -Dm644 systemd-user.pam "$pkgdir"/etc/pam.d/systemd-user

    # script to allow unprivileged user logins without systemd-logind
    #install -Dm644 systemd-user-sessions "$pkgdir"/lib/systemd/systemd-user-sessions
}

package_libsystemd() {
    pkgdesc="systemd client libraries"
    license=('GPL2')
    provides=('libsystemd.so' 'libudev.so')

    install -v -dm755 "$pkgdir"/usr/{lib,share/pkgconfig}
    install -v -dm755 "$pkgdir"/lib/
    mv -v libsystemd "$pkgdir"/lib
    mv -v "$pkgdir"/lib/libsystemd/* "$pkgdir"/lib/
    rm -rv "$pkgdir"/lib/libsystemd
    install build/src/libsystemd/libsystemd.pc ${pkgdir}/usr/share/pkgconfig/
}

package_systemd-sysvcompat() {
    pkgdesc="sysvinit compat for systemd"
    license=('GPL2')
    groups=('core')
    conflicts=('sysvinit')
    depends=('systemd')

    [[ -d "$pkgdir"/usr/share/man ]] && install -Dm644 -t "$pkgdir"/usr/share/man/man8 \
                                                build/man/{telinit,halt,reboot,poweroff,runlevel,shutdown}.8

    install -dm755 "$pkgdir"/sbin
    ln -s ../lib/systemd/systemd "$pkgdir"/sbin/init
    for tool in runlevel reboot shutdown poweroff halt telinit; do
        ln -s systemctl "$pkgdir"/sbin/$tool
    done
}

md5sums=('76db8004647283b779234364cd637d3c'
         '2e3f795bc0818e161c03b8cae9c99e80'
         '994074b6494dfa9a0e8510e689ce840b'
         'b93ecc3e0a95aebe63c69188840e29fd'
         '80fd2a6ec5ade81ebe4ea7ecce0f9b1b'
         '981d3730f7d14ac9459aba2753ce6a97'
         '11d703a47115c3ad4530fe78ae6036d1'
         'ce4c85b7eba8d857ac994b9e6f56c614'
         '3da33b055e52e6d34361b43b66b70f2e'
         'e2eb21708d789c44a0c21d47e2aa6d88'
         'f1f5ba23c19de5789eae870bbb281f62'
         'ffaf428ee9e0768860eee559c6d3f466'
         '55413a4f04445c0aaad203bc609fb22e'
         'e45e1b18f66a6c8354f21135b2168272'
         '6f7f8dec3702ff6a994b16ecc6e2af5c')
