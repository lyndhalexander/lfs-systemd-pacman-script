# -*-sh-*-

_hkgname=unliftio
pkgname=haskell-unliftio
pkgver=0.2.7.0
pkgrel=1
pkgdesc="The MonadUnliftIO typeclass for unlifting monads to IO (batteries included)"
url="https://github.com/fpco/monad-unlift"
license=("MIT")
arch=('x86_64')
depends=('ghc-libs' 'haskell-async' 'haskell-unliftio-core')
makedepends=('ghc' 'haskell-hspec')
source=("https://hackage.haskell.org/packages/archive/${_hkgname}/${pkgver}/${_hkgname}-${pkgver}.tar.gz")
sha512sums=('e4f805e8b22460b7a2eb9370eb3c474fefc8ccb9d13eaf78f95e4602521645547e0187b3a6de49e574b66a40b9bde132f7dc567f5ec75a273772f972cd5e9214')

build() {
    cd $_hkgname-$pkgver

    runhaskell Setup configure -O --enable-shared --enable-executable-dynamic --disable-library-vanilla \
               --prefix=/usr --docdir="/usr/share/doc/${pkgname}" --enable-tests \
               --dynlibdir=/usr/lib --libsubdir=\$compiler/site-local/\$pkgid
    runhaskell Setup build
    runhaskell Setup register --gen-script
    runhaskell Setup unregister --gen-script
    sed -i -r -e "s|ghc-pkg.*update[^ ]* |&'--force' |" register.sh
    sed -i -r -e "s|ghc-pkg.*unregister[^ ]* |&'--force' |" unregister.sh
}

check() {
    cd $_hkgname-$pkgver
    runhaskell Setup test
}

package() {
    cd $_hkgname-$pkgver

    install -D -m744 register.sh   "${pkgdir}/usr/share/haskell/register/${pkgname}.sh"
    install -D -m744 unregister.sh "${pkgdir}/usr/share/haskell/unregister/${pkgname}.sh"
    runhaskell Setup copy --destdir="${pkgdir}"
    install -D -m644 "LICENSE" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
    rm -f "${pkgdir}/usr/share/doc/${pkgname}/LICENSE"
}
