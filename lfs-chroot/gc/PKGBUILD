# -*-sh-*-
# Beyond Linux® From Scratch (systemd edition) - Version 2018-06-12
# Chapter 13. Programming
# http://www.linuxfromscratch.org/blfs/view/systemd/general/gc.html
# https://git.archlinux.org/svntogit/packages.git/tree/trunk/PKGBUILD?h=packages/gc

pkgname=gc
pkgver=7.6.4
pkgrel=1
pkgdesc="A garbage collector for C and C++"
arch=('x86_64')
url="http://www.hboehm.info/gc/"
license=('GPL')
depends=('gcc-libs' 'libatomic_ops')
groups=('programming')
source=(https://github.com/ivmai/bdwgc/releases/download/v${pkgver}/${pkgname}-${pkgver}.tar.gz)
sha256sums=('b94c1f2535f98354811ee644dccab6e84a0cf73e477ca03fb5a3758fb1fecd1c')

prepare() {
    cd ${pkgname}-${pkgver}
    ./configure --prefix=/usr --enable-cplusplus --disable-static --docdir=/usr/share/doc/${pkgname}-${pkgver}
    sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
}

build() {
    cd ${pkgname}-${pkgver}
    make
}

check() {
    cd ${pkgname}-${pkgver}
    make check
}

package() {
    cd ${pkgname}-${pkgver}
    make DESTDIR="${pkgdir}" install

    sed 's|GC_MALLOC 1L|gc 3|g' doc/gc.man |
        install -Dm644 /dev/stdin "${pkgdir}/usr/share/man/man3/gc.3"
}
