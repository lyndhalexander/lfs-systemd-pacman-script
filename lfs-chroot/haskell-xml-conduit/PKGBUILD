# -*-sh-*-


_hkgname=xml-conduit
pkgname=haskell-xml-conduit
pkgver=1.8.0
pkgrel=1
pkgdesc="Pure-Haskell utilities for dealing with XML with the conduit package."
url="https://github.com/snoyberg/xml"
license=("MIT")
arch=('x86_64')
depends=('ghc-libs' 'haskell-attoparsec' 'haskell-blaze-markup' 'haskell-conduit'
         'haskell-conduit-extra' 'haskell-data-default-class' 'haskell-monad-control'
         'haskell-resourcet' 'haskell-blaze-html' 'haskell-xml-types')
makedepends=('ghc' 'haskell-hspec' 'haskell-hunit')
source=("https://hackage.haskell.org/packages/archive/${_hkgname}/${pkgver}/${_hkgname}-${pkgver}.tar.gz")
sha512sums=('d6ffdba6db52cef4d11c8f7a49f357c3698673e8f5648c6f43510c5d55fcb0a7ba84e03340f7c84260bbeb026641a16e14d420b07f4845425d001462641c3bcd')

build() {
    cd $_hkgname-$pkgver

    runhaskell Setup configure -O --enable-shared --enable-executable-dynamic --disable-library-vanilla \
               --prefix=/usr --docdir="/usr/share/doc/${pkgname}" --enable-tests \
               --dynlibdir=/usr/lib --libsubdir=\$compiler/site-local/\$pkgid
    runhaskell Setup build
    runhaskell Setup register --gen-script
    runhaskell Setup unregister --gen-script
    sed -i -r -e "s|ghc-pkg.*update[^ ]* |&'--force' |" register.sh
    sed -i -r -e "s|ghc-pkg.*unregister[^ ]* |&'--force' |" unregister.sh
}

check() {
    cd $_hkgname-$pkgver
    runhaskell Setup test
}

package() {
    cd $_hkgname-$pkgver

    install -D -m744 register.sh   "${pkgdir}/usr/share/haskell/register/${pkgname}.sh"
    install -D -m744 unregister.sh "${pkgdir}/usr/share/haskell/unregister/${pkgname}.sh"
    runhaskell Setup copy --destdir="${pkgdir}"
    install -D -m644 "LICENSE" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
    rm -f "${pkgdir}/usr/share/doc/${pkgname}/LICENSE"
}
