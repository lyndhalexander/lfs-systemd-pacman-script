# -*-sh-*-

_hkgname=tree-diff
pkgname=haskell-tree-diff
pkgver=0.0.1
pkgrel=1
pkgdesc="Diffing of (expression) trees"
url="https://github.com/phadej/tree-diff"
license=('custom:BSD3')
arch=('x86_64')
depends=('ghc-libs' 'haskell-aeson' 'haskell-ansi-wl-pprint' 'haskell-ansi-terminal'
         'haskell-base-compat' 'haskell-generics-sop' 'haskell-hashable' 'haskell-memotrie'
         'haskell-parsers' 'haskell-quickcheck' 'haskell-scientific' 'haskell-tagged'
         'haskell-unordered-containers' 'haskell-uuid-types' 'haskell-vector')
makedepends=('ghc' 'haskell-trifecta' 'haskell-tasty' 'haskell-tasty-golden'
             'haskell-tasty-quickcheck')
source=(https://hackage.haskell.org/packages/archive/${_hkgname}/${pkgver}/${_hkgname}-${pkgver}.tar.gz)
sha512sums=('f0d123d6c65881c35ae18604648ebe3165ce200f590154f6ee0e35921a52e8ac6c7f641eb2498793f6943d77a7921da8a2f06800f489d514d0c0a639fb35e11f')

prepare() {
    cd $_hkgname-$pkgver
    sed -i -e 's/<.*2.11/<3/' -e 's/<.*4.11/<5/' -e 's/<.*1.3/<2/' -e 's/<.*0.10/<1/' -e 's/<.*1.1/<2/' $_hkgname.cabal
}

build() {
    cd $_hkgname-$pkgver

    runhaskell Setup configure -O --enable-shared --enable-executable-dynamic --disable-library-vanilla \
               --prefix=/usr --docdir="/usr/share/doc/${pkgname}" --enable-tests \
               --dynlibdir=/usr/lib --libsubdir=\$compiler/site-local/\$pkgid
    runhaskell Setup build
    runhaskell Setup register --gen-script
    runhaskell Setup unregister --gen-script
    sed -i -r -e "s|ghc-pkg.*update[^ ]* |&'--force' |" register.sh
    sed -i -r -e "s|ghc-pkg.*unregister[^ ]* |&'--force' |" unregister.sh
}

check() {
    cd $_hkgname-$pkgver
    runhaskell Setup test
}

package() {
    cd $_hkgname-$pkgver

    install -D -m744 register.sh   "${pkgdir}/usr/share/haskell/register/${pkgname}.sh"
    install -D -m744 unregister.sh "${pkgdir}/usr/share/haskell/unregister/${pkgname}.sh"
    runhaskell Setup copy --destdir="${pkgdir}"
    install -D -m644 "LICENSE" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
    rm -f "${pkgdir}/usr/share/doc/${pkgname}/LICENSE"
}
