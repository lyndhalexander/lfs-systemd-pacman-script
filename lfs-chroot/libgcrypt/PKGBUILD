# -*-sh-*-
# Beyond Linux® From Scratch (systemd edition) - Version 2018-06-15
# Chapter 9. General Libraries
# http://www.linuxfromscratch.org/blfs/view/systemd/general/libgcrypt.html
# https://git.archlinux.org/svntogit/packages.git/tree/trunk/PKGBUILD?h=packages/libgcrypt

pkgname=libgcrypt
pkgver=1.8.3
pkgrel=1
pkgdesc="General purpose cryptographic library based on the code from GnuPG"
arch=(x86_64)
url="http://www.gnupg.org"
license=('LGPL')
depends=('libgpg-error')
options=('!emptydirs')
# https://www.gnupg.org/download/integrity_check.html
source=(https://gnupg.org/ftp/gcrypt/${pkgname}/${pkgname}-${pkgver}.tar.bz2{,.sig})
sha1sums=('13bd2ce69e59ab538e959911dfae80ea309636e3'
          'SKIP')
validpgpkeys=('031EC2536E580D8EA286A9F22071B08A33BD3F06' # "NIIBE Yutaka (GnuPG Release Key) <gniibe@fsij.org>"
              'D8692123C4065DEA5E0F3AB5249B39D24F25E3B6') # Werner Koch
options=(!makeflags)

prepare() {
    cd ${pkgname}-${pkgver}
    # tests fail due to systemd+libseccomp preventing memory syscalls when building in chroots
    #  t-secmem: line 176: gcry_control (GCRYCTL_INIT_SECMEM, pool_size, 0) failed: General error
    #  FAIL: t-secmem
    #  t-sexp: line 1174: gcry_control (GCRYCTL_INIT_SECMEM, 16384, 0) failed: General error
    #  FAIL: t-sexp
    sed -i "s:t-secmem::" tests/Makefile.am
    sed -i "s:t-sexp::" tests/Makefile.am
    autoreconf -vfi
    ./configure --prefix=/usr \
	        --disable-static \
	        --disable-padlock-support
}

build() {
    cd ${pkgname}-${pkgver}
    make
}

check() {
    cd ${pkgname}-${pkgver}
    make check || true
}

package() {
    cd ${pkgname}-${pkgver}
    make DESTDIR=${pkgdir} install
}
